#!/bin/bash

#rails generate model Post title:string content:string
#rails generate model Tag title:string
#rails generate model Poststag post_id:integer tag_id:integer

if [ -f ./app/models/quot.rb ] ; then
  rails destroy model Quot
  rails destroy model Quotdetail
  rm db/development.sqlite3
  rm db/test.sqlite3
fi

rails generate scaffold Quot       quot_number customer representative issued:date consumption_tax:integer total_amount:integer
rails generate scaffold Quotdetail quot_number product_name unit_price:integer unit quantity:integer extended_price:integer quot:belongs_to
#rails generate model Quot       quot_number customer representative issued:date consumption_tax:integer total_amount:integer
#rails generate model Quotdetail quot_number product_name unit_price:integer unit quantity:integer extended_price:integer

rake db:create
rake db:migrate

# QuotApp
# -------
# 
# Quotation_Header
#  Quotation_Number
#  Customer
#  Representative
#  Issued_Date
#  Consumption_tax
#  Total_amount
# 
# Quotation_Detail
#  Quotation_Number
#  Product_Name
#  Unit_price
#  Unit
#  Quantity
#  Extended_Price

