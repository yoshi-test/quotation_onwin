# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
require "csv"

#DB reset command
#rake db:migrate:reset

quot_data = CSV.read('db/quots.csv')
first_line = true

quot_data.each do |quot_number,customer,representative,issued,consumption_tax,total_amount,product_name,unit_price,unit,quantity,extended_price|
  if (first_line) then
    first_line = false
  elsif
    header = Quot.create(
      :quot_number     => quot_number,     :customer     => customer, 
      :representative  => representative,  :issued       => issued,
      :consumption_tax => consumption_tax, :total_amount => total_amount)
    detail = Quotdetail.create(
      :quot_number    => quot_number,     :product_name   => product_name,
      :unit_price     => unit_price,      :unit           => unit,
      :quantity       => quantity,        :extended_price => extended_price,
      :quot           => header)
  end
end
