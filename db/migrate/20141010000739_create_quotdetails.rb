class CreateQuotdetails < ActiveRecord::Migration
  def change
    create_table :quotdetails do |t|
      t.string :quot_number
      t.string :product_name
      t.integer :unit_price
      t.string :unit
      t.integer :quantity
      t.integer :extended_price
      t.belongs_to :quot, index: true

      t.timestamps
    end
  end
end
