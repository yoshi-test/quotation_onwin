class CreateQuots < ActiveRecord::Migration
  def change
    create_table :quots do |t|
      t.string :quot_number
      t.string :customer
      t.string :representative
      t.date :issued
      t.integer :consumption_tax
      t.integer :total_amount

      t.timestamps
    end
  end
end
