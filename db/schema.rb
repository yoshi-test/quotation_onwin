# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141010000739) do

  create_table "quotdetails", force: true do |t|
    t.string   "quot_number"
    t.string   "product_name"
    t.integer  "unit_price"
    t.string   "unit"
    t.integer  "quantity"
    t.integer  "extended_price"
    t.integer  "quot_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "quotdetails", ["quot_id"], name: "index_quotdetails_on_quot_id"

  create_table "quots", force: true do |t|
    t.string   "quot_number"
    t.string   "customer"
    t.string   "representative"
    t.date     "issued"
    t.integer  "consumption_tax"
    t.integer  "total_amount"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
