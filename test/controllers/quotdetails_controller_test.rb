require 'test_helper'

class QuotdetailsControllerTest < ActionController::TestCase
  setup do
    @quotdetail = quotdetails(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:quotdetails)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create quotdetail" do
    assert_difference('Quotdetail.count') do
      post :create, quotdetail: { extended_price: @quotdetail.extended_price, product_name: @quotdetail.product_name, quantity: @quotdetail.quantity, quot_id: @quotdetail.quot_id, quot_number: @quotdetail.quot_number, unit: @quotdetail.unit, unit_price: @quotdetail.unit_price }
    end

    assert_redirected_to quotdetail_path(assigns(:quotdetail))
  end

  test "should show quotdetail" do
    get :show, id: @quotdetail
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @quotdetail
    assert_response :success
  end

  test "should update quotdetail" do
    patch :update, id: @quotdetail, quotdetail: { extended_price: @quotdetail.extended_price, product_name: @quotdetail.product_name, quantity: @quotdetail.quantity, quot_id: @quotdetail.quot_id, quot_number: @quotdetail.quot_number, unit: @quotdetail.unit, unit_price: @quotdetail.unit_price }
    assert_redirected_to quotdetail_path(assigns(:quotdetail))
  end

  test "should destroy quotdetail" do
    assert_difference('Quotdetail.count', -1) do
      delete :destroy, id: @quotdetail
    end

    assert_redirected_to quotdetails_path
  end
end
