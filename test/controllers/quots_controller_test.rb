require 'test_helper'

class QuotsControllerTest < ActionController::TestCase
  setup do
    @quot = quots(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:quots)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create quot" do
    assert_difference('Quot.count') do
      post :create, quot: { consumption_tax: @quot.consumption_tax, customer: @quot.customer, issued: @quot.issued, quot_number: @quot.quot_number, representative: @quot.representative, total_amount: @quot.total_amount }
    end

    assert_redirected_to quot_path(assigns(:quot))
  end

  test "should show quot" do
    get :show, id: @quot
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @quot
    assert_response :success
  end

  test "should update quot" do
    patch :update, id: @quot, quot: { consumption_tax: @quot.consumption_tax, customer: @quot.customer, issued: @quot.issued, quot_number: @quot.quot_number, representative: @quot.representative, total_amount: @quot.total_amount }
    assert_redirected_to quot_path(assigns(:quot))
  end

  test "should destroy quot" do
    assert_difference('Quot.count', -1) do
      delete :destroy, id: @quot
    end

    assert_redirected_to quots_path
  end
end
