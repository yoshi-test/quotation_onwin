class QuotdetailsController < ApplicationController
  before_action :set_quotdetail, only: [:show, :edit, :update, :destroy]

  # GET /quotdetails
  # GET /quotdetails.json
  def index
    @quotdetails = Quotdetail.all
  end

  # GET /quotdetails/1
  # GET /quotdetails/1.json
  def show
  end

  # GET /quotdetails/new
  def new
    @quotdetail = Quotdetail.new
  end

  # GET /quotdetails/1/edit
  def edit
  end

  # POST /quotdetails
  # POST /quotdetails.json
  def create
    @quotdetail = Quotdetail.new(quotdetail_params)

    respond_to do |format|
      if @quotdetail.save
        format.html { redirect_to @quotdetail, notice: 'Quotdetail was successfully created.' }
        format.json { render :show, status: :created, location: @quotdetail }
      else
        format.html { render :new }
        format.json { render json: @quotdetail.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /quotdetails/1
  # PATCH/PUT /quotdetails/1.json
  def update
    respond_to do |format|
      if @quotdetail.update(quotdetail_params)
        format.html { redirect_to @quotdetail, notice: 'Quotdetail was successfully updated.' }
        format.json { render :show, status: :ok, location: @quotdetail }
      else
        format.html { render :edit }
        format.json { render json: @quotdetail.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /quotdetails/1
  # DELETE /quotdetails/1.json
  def destroy
    @quotdetail.destroy
    respond_to do |format|
      format.html { redirect_to quotdetails_url, notice: 'Quotdetail was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_quotdetail
      @quotdetail = Quotdetail.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def quotdetail_params
      params.require(:quotdetail).permit(:quot_number, :product_name, :unit_price, :unit, :quantity, :extended_price, :quot_id)
    end
end
