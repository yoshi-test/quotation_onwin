class QuotsController < ApplicationController
  before_action :set_quot, only: [:show, :edit, :update, :destroy]

  # GET /quots
  # GET /quots.json
  def index
    @quots = Quot.all
  end

  # GET /quots/1
  # GET /quots/1.json
  def show
  end

  # GET /quots/new
  def new
    @quot = Quot.new
  end

  # GET /quots/1/edit
  def edit
  end

  # POST /quots
  # POST /quots.json
  def create
    @quot = Quot.new(quot_params)

    respond_to do |format|
      if @quot.save
        format.html { redirect_to @quot, notice: 'Quot was successfully created.' }
        format.json { render :show, status: :created, location: @quot }
      else
        format.html { render :new }
        format.json { render json: @quot.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /quots/1
  # PATCH/PUT /quots/1.json
  def update
    respond_to do |format|
      if @quot.update(quot_params)
        format.html { redirect_to @quot, notice: 'Quot was successfully updated.' }
        format.json { render :show, status: :ok, location: @quot }
      else
        format.html { render :edit }
        format.json { render json: @quot.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /quots/1
  # DELETE /quots/1.json
  def destroy
    @quot.destroy
    respond_to do |format|
      format.html { redirect_to quots_url, notice: 'Quot was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_quot
      @quot = Quot.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def quot_params
      params.require(:quot).permit(:quot_number, :customer, :representative, :issued, :consumption_tax, :total_amount)
    end
end
