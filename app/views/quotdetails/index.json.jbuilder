json.array!(@quotdetails) do |quotdetail|
  json.extract! quotdetail, :id, :quot_number, :product_name, :unit_price, :unit, :quantity, :extended_price, :quot_id
  json.url quotdetail_url(quotdetail, format: :json)
end
