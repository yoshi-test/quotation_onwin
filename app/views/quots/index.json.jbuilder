json.array!(@quots) do |quot|
  json.extract! quot, :id, :quot_number, :customer, :representative, :issued, :consumption_tax, :total_amount
  json.url quot_url(quot, format: :json)
end
